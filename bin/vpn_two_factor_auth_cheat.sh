#!/bin/bash

export VPNBASEDIR=${VPNBASEDIR:-${HOME}/vpn}
OATHTOOL=/usr/bin/oathtool
EXPECT=/usr/bin/expect
export SUDO=/usr/bin/sudo
export OPENVPN=/usr/sbin/openvpn

msg() {
    echo "$@" >&2
}

die() {
    msg "$@"
    exit 1
}

msg "VPNBASEDIR=${VPNBASEDIR}"

[[ -r ${VPNBASEDIR}/up ]] || die "*** Cannot read file: ${VPNBASEDIR}/up"
{ read user; read pass; } <<EOM
$(cat ${VPNBASEDIR}/up)
EOM

[[ -z "$1" ]] && die "usage: ${0##.*/} <datacenter>"

export user pass
export datacenter="$1"

[[ -r ${VPNBASEDIR}/up ]] || die "*** Cannot read file: $VPNBASEDIR/$datacenter/google_auth"
export token=$($OATHTOOL --totp -b $(cat $VPNBASEDIR/$datacenter/google_auth))
[[ -z "$token" ]] \
    && die "*** Unable to generate token based on the file $VPNBASEDIR/$datacenter/google_auth"

$EXPECT -c '
  spawn $env(SUDO) $env(OPENVPN) --config $env(VPNBASEDIR)/$env(datacenter)/client.ovpn
  expect "Enter Auth Username:"
  send "$env(user)\r"
  expect "Enter Auth Password:"
  send "$env(pass)\r"
  expect "Response:"
  send "$env(token)\r"
  interact
  expect eof'

